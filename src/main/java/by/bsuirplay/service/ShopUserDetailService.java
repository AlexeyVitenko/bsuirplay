//Source file: C:\\gen\\by\\bsuirplay\\service\\ShopUserDetailService.java

package by.bsuirplay.service;

import by.bsuirplay.dao.IUserDao;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class ShopUserDetailService implements UserDetailsService {
	private IUserDao userDao;
	
	@Override 
    /**
     * @param arg0
     * @return org.springframework.security.core.userdetails.UserDetails
     * @throws org.springframework.security.core.userdetails.UsernameNotFoundException
     * @roseuid 52A767A40123
     */
	public UserDetails loadUserByUsername(String arg0)
			throws UsernameNotFoundException {
		UserDetails userDetails = userDao.getUser(arg0);
		return userDetails;
	}
	   
    /**
     * @param arg0
     * @roseuid 52A767A40131
     */
	public void setUserDao(IUserDao arg0) {
		this.userDao = arg0;
	}
}
