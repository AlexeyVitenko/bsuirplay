//Source file: C:\\gen\\by\\bsuirplay\\service\\AppsService.java

package by.bsuirplay.service;

import java.util.List;

import by.bsuirplay.dao.IAppsDao;
import by.bsuirplay.model.App;

public class AppsService {
    
    private IAppsDao appsDao;
    
    /**
     * @return java.util.List
     * @roseuid 52A767A203C8
     */
    public List<App> getAppsList() {
        return appsDao.getAppsList();
    }
    
    /**
     * @return java.util.List
     * @roseuid 52A767A203C9
     */    
    public List<App> getPublishedAppsList() {
        return appsDao.getPublishedAppsList();
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A203CA
     */
    public void setAppsDao(IAppsDao arg0) {
        appsDao = arg0;
    }
    
    /**
     * @return java.util.List
     * @roseuid 52A767A203CC
     */
    public List<App> getUnpublishedAppsList() {
        return appsDao.getUnpublishedAppsList();
    }
    
    /**
     * @param arg0
     * @param arg1
     * @roseuid 52A767A203CD
     */
    public void publish(App arg0, boolean arg1) {
        arg0.setPublished(arg1);
        arg0.setInappropriate(false);
        appsDao.updateApp(arg0);
    }

    /**
     * @param arg0
     * @roseuid 52A767A203D0
     */
    public void inappropriate(App arg0) {
        arg0.setInappropriate(true);
        appsDao.updateApp(arg0);
    }

    /**
     * @param arg0
     * @roseuid 52A767A203D2
     */
    public void upload(App arg0) {
        arg0.setInappropriate(false);
        arg0.setPublished(false);
        appsDao.insertApp(arg0);
    }

    /**
     * @param arg0
     * @return java.util.List
     * @roseuid 52A767A203D4
     */
    public List<App> searchApps(String arg0) {
        return appsDao.searchAppsList(arg0, 10);
    }

    /**
     * @param arg0
     * @param arg1
     * @roseuid 52A767A203D7
     */
    public void buy(String arg0, App arg1) {
        arg1.addOwner(arg0);
        appsDao.updateApp(arg1);
    }

}
