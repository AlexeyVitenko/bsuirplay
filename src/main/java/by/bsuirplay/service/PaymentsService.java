//Source file: C:\\gen\\by\\bsuirplay\\service\\PaymentsService.java

package by.bsuirplay.service;

import java.util.List;

import by.bsuirplay.dao.IPaymentsDao;
import by.bsuirplay.model.App;
import by.bsuirplay.model.Payment;

public class PaymentsService {

    IPaymentsDao paymentsDao;

    /**
     * @param arg0
     * @roseuid 52A767A30185
     */
    public void setPaymentsDao(IPaymentsDao arg0) {
        this.paymentsDao = arg0;
    }
    
    /**
     * @return by.bsuirplay.dao.IPaymentsDao
     * @roseuid 52A767A30187
     */
    public IPaymentsDao getPaymentsDao() {
        return paymentsDao;
    }
    
    /**
     * @return java.util.List
     * @roseuid 52A767A30189
     */
    public List<Payment> getPaymentsList() {
        return paymentsDao.getPaymentsList();
    }
    
    /**
     * @param arg0
     * @param arg1
     * @return java.util.List
     * @roseuid 52A767A3018A
     */
    public List<Payment> getPaymentForPeriod(long arg0, long arg1) {
        return paymentsDao.getPaymentForPeriod(arg0, arg1);
    }
    
    /**
     * @param arg0
     * @param arg1
     * @roseuid 52A767A3018D
     */    
    public void buy(App arg0, String arg1) {
        Payment p = new Payment();
        p.setApp(arg0.getId());
        p.setAmount(arg0.getPrice());
        p.setOwner(arg1);
        p.setDate(System.currentTimeMillis());
        paymentsDao.buy(p);
    }

}
