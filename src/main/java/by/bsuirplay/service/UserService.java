//Source file: C:\\gen\\by\\bsuirplay\\service\\UserService.java

package by.bsuirplay.service;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import by.bsuirplay.dao.IUserDao;
import by.bsuirplay.model.ShopUser;

public class UserService {
	private IUserDao userDao;
	private static final Set<GrantedAuthority> USER_ROLE = new HashSet<>();
	private static final Set<GrantedAuthority> ADMIN_ROLE = new HashSet<>();
	
	static {
		USER_ROLE.add(new SimpleGrantedAuthority("ROLE_USER"));
		ADMIN_ROLE.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		ADMIN_ROLE.add(new SimpleGrantedAuthority("ROLE_USER"));
	}
   
	/**
     * @roseuid 52A767A20373
     */
	public UserService(){

	}
	 
    /**
     * @param arg0
     * @param arg1
     * @return boolean
     * @roseuid 52A767A20374
     */
	public boolean checkCredentials(String arg0, String arg1){
		ShopUser user = userDao.getUser(arg0);
		if (user != null) {
			if (StringUtils.equals(arg0, arg1)) {
				return true;
			}
		}
		return false;
	}
	   
    /**
     * @param arg0
     * @param arg1
     * @param arg2
     * @return boolean
     * @roseuid 52A767A20377
     */
	public boolean register(String arg0, String arg1, Set<GrantedAuthority> arg2){
		assert arg2 != null;
		
		ShopUser user = new ShopUser(arg0, arg1, arg2.isEmpty() ? USER_ROLE : arg2);
		return userDao.addUser(user);
	}
	
	/**
     * @param arg0
     * @roseuid 52A767A20387
     */
	public void setUserDao(IUserDao arg0){
		this.userDao = arg0;
	}
}