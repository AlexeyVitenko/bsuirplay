//Source file: C:\\gen\\by\\bsuirplay\\dao\\IPaymentsDao.java

package by.bsuirplay.dao;

import java.util.List;

import by.bsuirplay.model.App;
import by.bsuirplay.model.Payment;

public interface IPaymentsDao {
    public static final String APP = "app";
    public static final String OWNER = "owner";
    public static final String AMOUNT = "amount";
    public static final String DATE = "date";
    
    /**
     * @return java.util.List
     * @roseuid 52A767A102EF
     */
    public abstract List<Payment> getPaymentsList();

    /**
     * @param arg0
     * @param arg1
     * @return java.util.List
     * @roseuid 52A767A102F0
     */ 
    public abstract List<Payment> getPaymentForPeriod(long arg0, long arg1);

    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A102F3
     */
    public abstract boolean buy(Payment arg0);
}
