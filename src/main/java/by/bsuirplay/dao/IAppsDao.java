//Source file: C:\\gen\\by\\bsuirplay\\dao\\IAppsDao.java

package by.bsuirplay.dao;

import java.util.List;
import java.util.Vector;

import com.mongodb.DBCollection;

import by.bsuirplay.model.App;

public interface IAppsDao {    
    public static final String _ID = "_id";
    public static final String TITLE = "title";
    public static final String DESC = "desc";
    public static final String FREE = "free";
    public static final String PRICE = "price";
    public static final String DEV = "dev";
    public static final String PUBLISHED = "published";
    public static final String APKPATH = "apkpath";
    public static final String INAPPROPRIATE = "inappropriate";
    
    /**
     * @return java.util.Vector
     * @roseuid 52A767A10186
     */
    public abstract Vector<String> getReviews();
    
    /**
     * @return java.util.Vector
     * @roseuid 52A767A10187
     */
    public abstract Vector<App> getAppsList();
    
    /**
     * @param arg0
     * @return java.util.Vector
     * @roseuid 52A767A10188
     */
    public abstract Vector<App> getAppsByDeveloper(String arg0);
    
    /**
     * @return java.util.Vector
     * @roseuid 52A767A10190
     */
    public abstract Vector<App> getPublishedAppsList();
    
    /**
     * @param arg0
     * @roseuid 52A767A10191
     */
    public abstract void setAppsCollection(DBCollection arg0);
    
    /**
     * @return java.util.Vector
     * @roseuid 52A767A101A4
     */
    public abstract Vector<App> getUnpublishedAppsList();
   
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A101A5
     */
    public abstract boolean updateApp(App arg0);
   
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A101A7
     */
    public abstract boolean insertApp(App arg0);
    
    /**
     * @param arg0
     * @param arg1
     * @return java.util.Vector
     * @roseuid 52A767A101A9
     */
    public abstract Vector<App> searchAppsList(String arg0, int arg1);
}