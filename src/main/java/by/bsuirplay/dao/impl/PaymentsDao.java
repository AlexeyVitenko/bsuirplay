//Source file: C:\\gen\\by\\bsuirplay\\dao\\impl\\PaymentsDao.java

package by.bsuirplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

import by.bsuirplay.dao.IAppsDao;
import by.bsuirplay.dao.IPaymentsDao;
import by.bsuirplay.model.App;
import by.bsuirplay.model.Payment;

public class PaymentsDao implements IPaymentsDao {
    private static final Logger LOG = Logger.getLogger(PaymentsDao.class);
    private PaymentWrapper wrapper = new PaymentWrapper();
    
    private DBCollection paymentsCollection;
    
    /**
     * @param arg0
     * @roseuid 52A767A30120
     */
    public void setPaymentsCollection(DBCollection arg0) {
        paymentsCollection = arg0;
    }

    /**
     * @param arg0
     * @return java.util.List
     * @roseuid 52A767A30122
     */
    private List<Payment> getPaymentsFromCursor(DBCursor arg0) {
        List<Payment> Payments = new ArrayList<>();
        try {
            List<DBObject> PaymentDocs = arg0.toArray();
            for (DBObject PaymentDoc : PaymentDocs) {
                Payment Payment = getPayment(PaymentDoc);
                Payments.add(Payment);
            }
        } catch (MongoException e) {
            LOG.error(e.getMessage());
        }
        return Payments;
    }
    
    /**
     * @param arg0
     * @return by.bsuirplay.model.Payment
     * @roseuid 52A767A30125
     */
    private Payment getPayment(DBObject arg0) {
        Payment payment = new Payment();
        payment.setApp(arg0.get(IPaymentsDao.APP).toString());
        payment.setAmount(arg0.get(IPaymentsDao.AMOUNT).toString());
        payment.setDate(Long.parseLong(arg0.get(IPaymentsDao.DATE).toString()));
        payment.setOwner(arg0.get(IPaymentsDao.OWNER).toString());
        return payment;
    }
    
    @Override
    /**
     * @return java.util.List
     * @roseuid 52A767A30127
     */
    public List<Payment> getPaymentsList() {
        DBCursor cursor = paymentsCollection.find();
        List<Payment> payments = getPaymentsFromCursor(cursor);
        return payments;
    }

    // db.payments.find({"$and":[{date:{"$gt":100}},{date:{"$lt":1386004475875}}]})

    @Override
    /**
     * @param arg0
     * @param arg1
     * @return java.util.List
     * @roseuid 52A767A30128
     */
    public List<Payment> getPaymentForPeriod(long arg0, long arg1) {
        BasicDBList list = new BasicDBList();
        list.put(0, new BasicDBObject(IPaymentsDao.DATE, new BasicDBObject("$gt", arg0)));
        list.put(1, new BasicDBObject(IPaymentsDao.DATE, new BasicDBObject("$lt", arg1)));
        DBCursor cursor = paymentsCollection.find(new BasicDBObject("$and", list));
        List<Payment> payments = getPaymentsFromCursor(cursor);
        return payments;
    }

    @Override
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A30130
     */
    public boolean buy(Payment arg0) {
        DBObject appDoc = getDocument(arg0);
        try {
            paymentsCollection.insert(appDoc);
            wrapper.transact(arg0.getOwner(), arg0.getApp(), arg0.getAmount());
        } catch (MongoException me) {
            return false;
        }
        return true;
    }
    
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A30132
     */
    public boolean refund(Payment arg0) {
        return true;
    }
    
    /**
     * @param arg0
     * @return com.mongodb.DBObject
     * @roseuid 52A767A30134
     */
    private DBObject getDocument(Payment arg0) {
        DBObject app = new BasicDBObject(IPaymentsDao.OWNER, arg0.getOwner());
        app.put(IPaymentsDao.AMOUNT, arg0.getAmount());
        app.put(IPaymentsDao.DATE, arg0.getDate());
        app.put(IPaymentsDao.APP, arg0.getApp());        
        return app;
    }
}
