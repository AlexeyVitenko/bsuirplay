//Source file: C:\\gen\\by\\bsuirplay\\dao\\impl\\UserDao.java

package by.bsuirplay.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import by.bsuirplay.dao.IUserDao;
import by.bsuirplay.model.ShopUser;
import by.bsuirplay.util.Utils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

public class UserDao implements IUserDao {
	private static final Logger LOG = Logger.getLogger(UserDao.class);
	private DBCollection usersCollection;
	
	/**
	 * @roseuid 52A767A200B5
	 */
	public UserDao() {

	}

	/**
	 * @param arg0
	 * @return by.bsuirplay.model.ShopUser
	 * @roseuid 52A767A200B6
	 */
	public ShopUser getUser(String arg0){
		ShopUser shopUser = null;
		DBObject user = usersCollection.findOne(new BasicDBObject("_id", arg0));
		if (user != null) {	
			Set<GrantedAuthority> authorities = new HashSet<>();
			BasicDBList roles = (BasicDBList) user.get("roles");
			for(Object role : roles) {
				authorities.add(new SimpleGrantedAuthority(role.toString()));
			}
			shopUser = new ShopUser(
					user.get("_id").toString(), 
					user.get("password").toString(), 
					authorities);
		}
		return shopUser;
	}

	/**
	 * @param arg0
	 * @roseuid 52A767A200B8
	 */	
	public void updateUser(ShopUser arg0){
		
	}
	 
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A200BA
     */
	public boolean addUser(ShopUser arg0){
		DBObject user = new BasicDBObject("_id", arg0.getUsername())
						.append("password", arg0.getPassword())
						.append("roles", Utils.getRoles(arg0.getAuthorities()));
		try {
			usersCollection.insert(user);
            return true;
        } catch (MongoException.DuplicateKey e) {
            LOG.info("Username already in use: " + arg0.getUsername());
            return false;
        }
	}

    /**
     * @param arg0
     * @roseuid 52A767A200BC
     */
	public void deleteUser(String arg0){
		usersCollection.remove(new BasicDBObject("_id", arg0));
	}

	/**
	 * @param arg0
	 * @roseuid 52A767A200BE
	 */
	public void setUsersCollection(DBCollection arg0) {
		this.usersCollection = arg0;
	}
}