//Source file: C:\\gen\\by\\bsuirplay\\dao\\impl\\AppsDao.java

package by.bsuirplay.dao.impl;

import by.bsuirplay.dao.IAppsDao;
import by.bsuirplay.dao.IPaymentsDao;
import by.bsuirplay.model.App;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

public class AppsDao implements IAppsDao {
    private static final Logger LOG = Logger.getLogger(AppsDao.class);
    private DBCollection appsCollection;

    @Override
    /**
     * @return java.util.Vector
     * @roseuid 52A767A201BF
     */
    public Vector<String> getReviews() {
        return null;
    }

    @Override
    /**
     * @return java.util.Vector
     * @roseuid 52A767A201C0
     */
    public Vector<App> getAppsList() {
        DBCursor cursor = appsCollection.find();
        Vector<App> apps = getAppsFromCursor(cursor);
        return apps;
    }
    
    @Override
    /**
     * @param arg0
     * @roseuid 52A767A201C1
     */
    public void setAppsCollection(DBCollection arg0) {
        appsCollection = arg0;
    }

    /**
     * @param arg0
     * @return java.util.Vector
     * @roseuid 52A767A201C3
     */
    private Vector<App> getAppsFromCursor(DBCursor arg0) {
        Vector<App> apps = new Vector<>();
        try {
            Vector<DBObject> appDocs = new Vector<DBObject>(arg0.toArray());
            for (DBObject appDoc : appDocs) {
                App app = getApp(appDoc);
                apps.add(app);
            }
        } catch (MongoException e) {
            LOG.error(e.getMessage());
        }
        return apps;
    }
    
    /**
     * @param arg0
     * @return by.bsuirplay.model.App
     * @roseuid 52A767A201CA
     */ 
    private App getApp(DBObject arg0) {
        App app = new App();
        app.setId(arg0.get("_id").toString());
        app.setTitle(arg0.get("title").toString());
        app.setDesc(arg0.get("desc").toString());
        app.setIsFree(arg0.get("free").toString());
        app.setPrice(arg0.get("price").toString());
        app.setDeveloper(arg0.get("dev").toString());
        app.setApkPath(arg0.get(IAppsDao.APKPATH).toString());
        app.setPublished(new Boolean(arg0.get(IAppsDao.PUBLISHED).toString()));
        app.setInappropriate(new Boolean(arg0.get(IAppsDao.INAPPROPRIATE).toString()));
        app.setOwners(new Vector<String>((AbstractCollection<String>)arg0.get(IPaymentsDao.OWNER)));
        return app;
    }

    @Override 
    /**
     * @param arg0
     * @return java.util.Vector
     * @roseuid 52A767A201D1
     */
    public Vector<App> getAppsByDeveloper(String arg0) {
        DBCursor cursor = appsCollection.find(new BasicDBObject(IAppsDao.DEV, arg0));
        Vector<App> apps = getAppsFromCursor(cursor);
        return apps;
    }

    @Override
    /**
     * @return java.util.Vector
     * @roseuid 52A767A201D3
     */
    public Vector<App> getPublishedAppsList() {
        DBCursor cursor = appsCollection.find(new BasicDBObject(IAppsDao.PUBLISHED, true));
        Vector<App> apps = getAppsFromCursor(cursor);
        return apps;
    }
    
    @Override
    /**
     * @return java.util.Vector
     * @roseuid 52A767A201D4
     */
    public Vector<App> getUnpublishedAppsList() {
        DBCursor cursor = appsCollection.find(new BasicDBObject(IAppsDao.PUBLISHED, false));
        Vector<App> apps = getAppsFromCursor(cursor);
        return apps;
    }

    @Override
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A201D5
     */
    public boolean updateApp(App arg0) {
        DBObject concertDoc = getDocument(arg0);
        DBObject query = new BasicDBObject("_id", concertDoc.get("_id"));
        try {
            appsCollection.update(query, concertDoc);
        } catch (MongoException.DuplicateKey e) {
            LOG.info(e.getMessage(), e);
            return false;
        }
        return true;
    }
    
    /**
     * @param arg0
     * @return com.mongodb.DBObject
     * @roseuid 52A767A201D7
     */
    private DBObject getDocument(App arg0) {
        DBObject app = new BasicDBObject(IAppsDao.TITLE, arg0.getTitle());
        app.put(IAppsDao.DESC, arg0.getDesc());
        app.put(IAppsDao.DEV, arg0.getDeveloper());
        app.put(IAppsDao.FREE, arg0.getIsFree());
        app.put(IAppsDao.PUBLISHED, arg0.isPublished());
        app.put(IAppsDao.PRICE, arg0.getPrice());
        app.put(IAppsDao.APKPATH, arg0.getApkPath());
        app.put(IAppsDao.INAPPROPRIATE, arg0.isInappropriate());
        app.put(IPaymentsDao.OWNER, arg0.getOwners());
        if (StringUtils.isBlank(arg0.getId()) == false) {
            app.put(IAppsDao._ID, arg0.getId());
        }
        
        return app;
    }
    
    @Override
    /**
     * @param arg0
     * @param arg1
     * @return java.util.Vector
     * @roseuid 52A767A201D9
     */
    public Vector<App> searchAppsList(String arg0, int arg1) {
        BasicDBObject query = new BasicDBObject();
        
        query.put(IAppsDao.TITLE, Pattern.compile(arg0));
        query.put(IAppsDao.PUBLISHED, true);
        DBCursor cursor = appsCollection.find(query).limit(arg1);
        
        Vector<App> concerts = getAppsFromCursor(cursor);
        return concerts;
    }

    @Override
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A201DC
     */
    public boolean insertApp(App arg0) {
        DBObject appDoc = getDocument(arg0);
        try {
            appsCollection.insert(appDoc);
        } catch (MongoException me) {
            return false;
        }
        return true;
    }
}