//Source file: C:\\gen\\by\\bsuirplay\\dao\\IUserDao.java

package by.bsuirplay.dao;

import by.bsuirplay.model.ShopUser;

public interface IUserDao {

    /**
     * @param arg0
     * @return by.bsuirplay.model.ShopUser
     * @roseuid 52A767A10117
     */
	public ShopUser getUser(String arg0);

    /**
     * @param arg0
     * @roseuid 52A767A1011E
     */
	public void updateUser(ShopUser arg0);

    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A10120
     */
	public boolean addUser(ShopUser arg0);
	
	/**
     * @param arg0
     * @roseuid 52A767A10122
     */
	public void deleteUser(String arg0);

}