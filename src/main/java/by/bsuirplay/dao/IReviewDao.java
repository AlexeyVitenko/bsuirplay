//Source file: C:\\gen\\by\\bsuirplay\\dao\\IReviewDao.java

package by.bsuirplay.dao;

import java.util.List;

import by.bsuirplay.model.App;
import by.bsuirplay.model.Review;
import by.bsuirplay.model.ShopUser;

public interface IReviewDao {
    /**
     * @param arg0
     * @return java.util.List
     * @roseuid 52A767A10272
     */
    public abstract List<Review> getReviews(App app);
    
    /**
     * @param arg0
     * @param arg1
     * @param arg2
     * @roseuid 52A767A10277
     */
    public void publish(String arg0, ShopUser arg1, App arg2);
}
