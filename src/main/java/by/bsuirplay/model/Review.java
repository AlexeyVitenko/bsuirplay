//Source file: C:\\gen\\by\\bsuirplay\\model\\Review.java

package by.bsuirplay.model;

import java.util.Date;

public class Review {

    private String message;
    private Date date;
    private String reviewer;
    private String app;
    
    /**
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     * @roseuid 52A767A10139
     */ 
    public Review(String arg0, Date arg1, String arg2, String arg3) {
        super();
        this.message = arg0;
        this.date = arg1;
        this.reviewer = arg2;
        this.app = arg3;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A1013E
     */
    public String getMessage() {
        return message;
    }
    
    /**
     * @return java.util.Date
     * @roseuid 52A767A1013F
     */
    public Date getDate() {
        return date;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10140
     */
    public String getReviewer() {
        return reviewer;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10141
     */
    public String getApp() {
        return app;
    }
    
    
    
}
