//Source file: C:\\gen\\by\\bsuirplay\\model\\App.java

package by.bsuirplay.model;

import java.util.ArrayList;
import java.util.List;

public class App {

    private String _id; 
    private String title;
    private String desc;
    private String price;
    private boolean isFree;
    private boolean isPublished;
    private boolean isInappropriate;
    private String developer;
    private String apkPath;
    private List<String> owners = new ArrayList<String>();
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10073
     */ 
    public String getId() {
        return _id;
    }
   
    /**
     * @param arg0
     * @roseuid 52A767A10074
     */
    public void setId(String arg0) {
        _id = arg0;
    }
    
    
    /**
     * @param arg0
     * @return boolean
     * @roseuid 52A767A10076
     */
    public boolean isOwner(String arg0) {
        return owners.contains(arg0);
    }

    /**
     * @return java.util.List
     * @roseuid 52A767A10078
     */
    public List<String> getOwners() {
        return owners;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10079
     */
    public String getApkPath() {
        return apkPath;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1007A
     */
    public void setApkPath(String arg0) {
        apkPath = arg0;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A1007C
     */
    public String getDeveloper() {
        return developer;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A1007D
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A1007E
     */
    public String getDesc() {
        return desc;
    }
    
    /**
     * @return java.util.List
     * @roseuid 52A767A10081
     */
    public List<String> getReviews() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * @return boolean
     * @roseuid 52A767A10082
     */
    public boolean getIsFree() {
        return isFree;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10083
     */
    public String getPrice() {
        return price;
    }
    
    /**
     * @return boolean
     * @roseuid 52A767A10084
     */
    public boolean isInappropriate() {
        return isInappropriate;
    }
    
    /**
     * @return boolean
     * @roseuid 52A767A10085
     */  
    public boolean isPublished() {
        return isPublished;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1008B
     */
    public void setTitle(String arg0) {
        title = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1008D
     */
    public void setDesc(String arg0) {
        desc = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1008F
     */
    public void setIsFree(String arg0) {
        isFree = new Boolean(arg0);
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A10091
     */
    public void setIsFree(boolean arg0) {
        isFree = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A10093
     */
    public void setPrice(String arg0) {
        price = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A10096
     */
    public void setDeveloper(String arg0) {
        developer = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A10098
     */
    public void setPublished(boolean arg0) {
        isPublished = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1009A
     */    
    public void setInappropriate(boolean arg0) {
        isInappropriate = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1009C
     */
    public void setOwners(List<String> arg0) {
        owners = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1009F
     */
    public void addOwner(String arg0) {
        owners.add(arg0);
    }
}
