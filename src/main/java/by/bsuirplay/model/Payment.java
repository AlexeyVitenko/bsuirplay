//Source file: C:\\gen\\by\\bsuirplay\\model\\Payment.java

package by.bsuirplay.model;

import java.text.SimpleDateFormat;

public class Payment {
    
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    private String owner;
    private String amount;
    private String app;
    private long date;    
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10251
     */
    public String getOwner() {
        return owner;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10252
     */
    public String getAmount() {
        return amount;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10253
     */
    public String getApp() {
        return app;
    }
    
    /**
     * @return long
     * @roseuid 52A767A10254
     */
    public long getDate() {
        return date;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A10258
     */
    public String getDateFormated() {
        return sdf.format(date);
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A10259
     */   
    public void setOwner(String arg0) {
        owner = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1025B
     */
    public void setDate(long arg0) {
        date = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1025D
     */
    public void setAmount(String arg0) {
        amount = arg0;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A1025F
     */
    public void setApp(String arg0) {
        app = arg0;
    }
}
