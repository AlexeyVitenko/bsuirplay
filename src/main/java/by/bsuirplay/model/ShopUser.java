package by.bsuirplay.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class ShopUser extends User {

    /**
     * @param arg0
     * @param arg1
     * @param arg2
     * @roseuid 52A767A1029F
     */
    public ShopUser(String arg0, String arg1,
            Collection<? extends GrantedAuthority> arg2) {
        super(arg0, arg1, arg2);
    }
}
