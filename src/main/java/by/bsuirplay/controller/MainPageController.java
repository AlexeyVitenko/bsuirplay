//Source file: C:\\gen\\by\\bsuirplay\\controller\\MainPageController.java

package by.bsuirplay.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import by.bsuirplay.util.Utils;

@ViewScoped
@ManagedBean(name="mainPageBean")
@SessionScoped
public class MainPageController {
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A400C8
     */
    public String goToPage() {
        UserDetails  user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (Utils.hasRole(user.getAuthorities(), "ROLE_ADMIN")) {
            return "/pages/admin/admin";
        } else if (Utils.hasRole(user.getAuthorities(), "ROLE_USER")) {
            return "/pages/user/user";
        } else if (Utils.hasRole(user.getAuthorities(), "ROLE_DEVELOPER")) {
            return "/pages/developer/developer";
        }
        return null;
    }
    
    /**
     * @return boolean
     * @roseuid 52A767A400C9
     */
    public boolean isAuthenthicated() {
        return  SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails;
//        return Utils.hasRole(user.getAuthorities(), "ROLE_ADMIN") || 
//                Utils.hasRole(user.getAuthorities(), "ROLE_USER") || 
//                Utils.hasRole(user.getAuthorities(), "ROLE_DEVELOPER");
    }
    
    /**
     * @return boolean
     * @roseuid 52A767A400CA
     */
    public boolean isGuest() {
        return !isAuthenthicated();
    }
}
