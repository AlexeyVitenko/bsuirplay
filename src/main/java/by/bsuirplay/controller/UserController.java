//Source file: C:\\gen\\by\\bsuirplay\\controller\\UserController.java

package by.bsuirplay.controller;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;
import java.util.Collections;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import by.bsuirplay.service.UserService;
import by.bsuirplay.util.Utils;

/**
 * @author Alexey
 * @version 1.0
 * @created 09-Nov-2013 21:46:04
 */
@ManagedBean(name="userBean")
@SessionScoped
public class UserController implements Serializable {
	private static final long serialVersionUID = -8118462113323863022L;
	private static final Logger LOG = Logger.getLogger(UserController.class);
	private String password;
	private String username;
	
	@ManagedProperty(value="#{userService}")
	private UserService userService;
	
	@ManagedProperty(value="#{authenticationManager}")
    private AuthenticationManager authenticationManager;

	/**
    * @roseuid 52A767A4005C
    */
	public UserController() {
		
	}
	
	/**
    * @return java.lang.String
    * @roseuid 52A767A4005E
    */
	public String login(){
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(
            		this.getUsername(), this.getPassword());
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
            if (result.isAuthenticated() ) {
                if (Utils.hasRole(result.getAuthorities(), "ROLE_ADMIN")) {
                    return "/pages/admin/admin";
                } else if (Utils.hasRole(result.getAuthorities(), "ROLE_DEVELOPER")) {
                    return "/pages/developer/developer";
                } else if (Utils.hasRole(result.getAuthorities(), "ROLE_USER")) {
                    return "/pages/user/user";
                }
            }
        } catch (AuthenticationException e) {
        	LOG.info("Authentivation failed. " + e.getMessage());
        	FacesContext.getCurrentInstance().addMessage(null, 
        			new FacesMessage(FacesMessage.SEVERITY_ERROR,"Wrong credentials", "Username or password was incorrect"));  
        	return "/pages/home/home";
        }
        return "/pages/unsecure/apps";
	}

	/**
    * @roseuid 52A767A4005F
    */
	public void register(){
		boolean usernameNotBlank = isNotBlank(username);
		boolean passwordNotBlank = isNotBlank(password);
		if (usernameNotBlank && passwordNotBlank) {	
			userService.register(username, password, Collections.<GrantedAuthority>emptySet());
			FacesContext.getCurrentInstance().addMessage("username", 
        			new FacesMessage(FacesMessage.SEVERITY_INFO,"Success", "You are registered. Use login page next"));
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			if (!usernameNotBlank) {
				context.addMessage("username", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "Username cannot be blank"));
			}
			if (!passwordNotBlank) {
				context.addMessage("password", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "Password cannot be blank"));
			}
			FacesContext.getCurrentInstance().addMessage(null, 
        			new FacesMessage(FacesMessage.SEVERITY_ERROR,"Credentials required", "Username and password are required"));
		}
	}
	
	/**
    * @param arg0
    * @return boolean
    * @roseuid 52A767A40060
    */
	public boolean checkRole(String arg0) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal.getClass().equals(String.class)) {
			// if "anonimousUser" logged in
			String anonimousUser = (String) principal;
			return StringUtils.equals(anonimousUser, arg0);
		}
		UserDetails  user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		for (GrantedAuthority userRole : user.getAuthorities()) {
			if (userRole.getAuthority().equals(arg0)) {
				return true;
			}
		}
		return false;
	}
	
	/**
     * @return java.lang.String
     * @roseuid 52A767A40068
     */
	public String getPassword(){
		return this.password;
	}

	/**
     * @param arg0
     * @roseuid 52A767A40069
     */
	public void setPassword(String arg0){
		this.password = arg0;
	}

	/**
     * @return java.lang.String
     * @roseuid 52A767A4006B
     */
	public String getUsername(){
		return this.username;
	}

	/**
     * @param arg0
     * @roseuid 52A767A4006C
     */
	public void setUsername(String arg0){
		this.username = arg0;
	}

	/**
     * @param arg0
     * @roseuid 52A767A4006E
     */
	public void setUserService(UserService arg0){
		this.userService = arg0;
	}
	
	/**
     * @param arg0
     * @roseuid 52A767A40073
     */
	public void setAuthenticationManager(AuthenticationManager arg0) {
		this.authenticationManager = arg0;
	}
}