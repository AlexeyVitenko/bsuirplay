//Source file: C:\\gen\\by\\bsuirplay\\controller\\AppsController.java

package by.bsuirplay.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import by.bsuirplay.model.App;
import by.bsuirplay.service.AppsService;
import by.bsuirplay.service.PaymentsService;

/**
 * @author Alexey
 */
@ViewScoped
@ManagedBean(name="appsBean")
@SessionScoped
public class AppsController {
    private String title;
    private List<App> apps = new ArrayList<App>();
    @ManagedProperty(value="#{appsService}")
    private AppsService appsService;
    
    /**
     * @roseuid 52A767A30288
     */
    public AppsController() 
    {
     
    }
    
    @ManagedProperty(value="#paymentsService")
    private PaymentsService paymentsService;
    
    /**
     * @return by.bsuirplay.service.PaymentsService
     * @roseuid 52A767A30289
     */
    public PaymentsService getPaymentsService() {
        return paymentsService;
    }

    /**
     * @param arg0
     * @roseuid 52A767A3028A
     */
    public void setPaymentsService(PaymentsService arg0) {
        this.paymentsService = arg0;
    }

    /**
     * @return java.util.List
     * @roseuid 52A767A3028C
     */
    public List<App> getAppsList() {
        return appsService.getAppsList();
    }
    
    /**
     * @return java.util.List
     * @roseuid 52A767A3028D
     */
    public List<App> getPublishedAppsList() {
        return appsService.getPublishedAppsList();
    }
    
    /**
     * @return java.util.List
     * @roseuid 52A767A3028E
     */    
    public List<App> getUnpublishedAppsList() {
        return appsService.getUnpublishedAppsList();
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A3028F
     */
    public void setAppsService(AppsService serv) {
        appsService = serv;
    }

    /**
     * @param arg0
     * @param arg1
     * @roseuid 52A767A30291
     */
    public void publish(App arg0, boolean arg1) {
        appsService.publish(arg0, arg1);
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A30294
     */
    public void inappropriate(App arg0) {
        appsService.inappropriate(arg0);
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A30296
     */
    public void download(App arg0) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(arg0.getApkPath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A30298
     */
    public String search() {
        apps = appsService.searchApps(title);
        return "/pages/user/search";
    }
    
    /**
     * @param arg0
     * @return java.lang.String
     * @roseuid 52A767A30299
     */
    public String similar(App arg0) {
        apps = appsService.searchApps(arg0.getTitle().substring(0, arg0.getTitle().length() - 2));
        return "/pages/user/search";
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A3029B
     */
    public String toAddApp() {
        return "/pages/developer/addapp";
    }
    
    /**
     * @return java.util.List
     * @roseuid 52A767A3029C
     */
    public List<App> getApps() {
        return apps;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A3029D
     */
    public void setTitle(String arg0) {
        title = arg0;
    }
    
    /**
     * @return java.lang.String
     * @roseuid 52A767A302A1
     */    
    public String getTitle() {
        return title;
    }
    
    /**
     * @param arg0
     * @param arg1
     * @roseuid 52A767A302A2
     */
    public void buy(String arg0, App arg1) {
        appsService.buy(arg0, arg1);
        paymentsService.buy(arg1, arg0);
    }
    
    /**
     * @param arg0
     * @param arg1
     * @return boolean
     * @roseuid 52A767A302A5
     */
    public boolean downloadable(App arg0, String arg1) {
        return arg0.getIsFree() || arg0.getOwners().contains(arg1);
    }
    
    /**
     * @param arg0
     * @param arg1
     * @return boolean
     * @roseuid 52A767A302AB
     */
    public boolean needBuy(App arg0, String arg1) {
        return arg0.getIsFree() == false && arg0.getOwners().contains(arg1) == false;
    }
}
