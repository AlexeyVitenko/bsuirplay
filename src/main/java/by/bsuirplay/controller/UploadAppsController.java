//Source file: C:\\gen\\by\\bsuirplay\\controller\\UploadAppsController.java

package by.bsuirplay.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import by.bsuirplay.model.App;
import by.bsuirplay.service.AppsService;
import by.bsuirplay.service.ShopUserDetailService;
import by.bsuirplay.service.UserService;

/**
 * @author Alexey
 */
@ViewScoped
@ManagedBean(name="uploadBean")
@SessionScoped
public class UploadAppsController {
    private App app = new App();
    @ManagedProperty(value="#{appsService}")
    private AppsService appsService;
    
    /**
     * @param arg0
     * @roseuid 52A767A4018B
     */
    public void setAppsService(AppsService arg0) {
        appsService = arg0;
    }
    
    /**
     * @return by.bsuirplay.model.App
     * @roseuid 52A767A4018D
     */
    public App getApp() {
        return app;
    }
    
    /**
     * @param arg0
     * @return java.lang.String
     * @roseuid 52A767A4018E
     */
    public String upload(String arg0) {
        app.setDeveloper(arg0);
        app.setApkPath("/appstore-release.apk");
        appsService.upload(app);
        return "/pages/developer/developer";
    }

}
