//Source file: C:\\gen\\by\\bsuirplay\\controller\\ReportsController.java

package by.bsuirplay.controller;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import by.bsuirplay.dao.IPaymentsDao;
import by.bsuirplay.model.Payment;
import by.bsuirplay.service.PaymentsService;

@ViewScoped
@ManagedBean(name="reportsBean")
@SessionScoped
public class ReportsController {
    
    private Date startDate;
    private Date endDate;

    /**
     * @return java.util.Date
     * @roseuid 52A767A400A7
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param arg0
     * @roseuid 52A767A400A8
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return java.util.Date
     * @roseuid 52A767A400AA
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param arg0
     * @roseuid 52A767A400AB
     */
    public void setEndDate(Date arg0) {
        this.endDate = arg0;
    }

    @ManagedProperty(value="#paymentsService")
    private PaymentsService paymentsService;

    /**
     * @return by.bsuirplay.service.PaymentsService
     * @roseuid 52A767A400AD
     */
    public PaymentsService getPaymentsService() {
        return paymentsService;
    }

    /**
     * @return java.lang.String
     * @roseuid 52A767A400AE
     */
    public String create() {
        if (startDate != null && endDate != null) {
            return "/pages/admin/reportslist";
        }
        return null;
    }
    
    /**
     * @param arg0
     * @roseuid 52A767A400AF
     */
    public void setPaymentsService(PaymentsService arg0) {
        this.paymentsService = arg0;
    }

    /**
     * @return java.util.List
     * @roseuid 52A767A400B1
     */
    public List<Payment> getPaymentsList() {
        return paymentsService.getPaymentsList();
    }

    /**
     * @return java.util.List
     * @roseuid 52A767A400B2
     */
    public List<Payment> getPaymentForPeriod() {
        return paymentsService.getPaymentForPeriod(startDate.getTime(), endDate.getTime());
    }
}
