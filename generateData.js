use bsuirplay;
db.apps.drop();
db.users.drop();
db.payments.drop();
dev = {
    "_id":"main",
    "password":"main",
    "roles":["ROLE_DEVELOPER"]
};
db.users.insert(dev)
dev = {
    "_id":"admin",
    "password":"admin",
    "roles":["ROLE_ADMIN"]
};
db.users.insert(dev)
dev = {
    "_id":"user",
    "password":"user",
    "roles":["ROLE_USER"]
};
db.users.insert(dev)
app = {
    "_id":"com.main.superapp",
    "title":"Superapp",
    "desc":"blablabla",
    "free":true,
    "price":"100500",
    "apkpath":"file:///home/avitenko/Downloads/appstore-release.apk",
    "dev":"main",
    "published":true,
    "inappropriate":false,
    "owner":[]
};
db.apps.insert(app)
db.users.insert(dev)
app = {
    "_id":"com.main.superapp2",
    "title":"Superapp2",
    "desc":"blablabla",
    "free":false,
    "price":"100",
    "apkpath":"file:///home/avitenko/Downloads/appstore-release.apk",
    "dev":"main",
    "published":false,
    "inappropriate":false,
    "owner":["user"]
};
db.apps.insert(app)
db.apps.ensureIndex({"title":1}, {unique:true});

payment = {
    "app":"com.main.superapp2",
    "owner":"user",
    "amount":"100",
    "date":"1385922460"
}
db.payments.insert(payment)
